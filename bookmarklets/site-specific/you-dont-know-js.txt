javascript: (() => { var targetAll = document.querySelectorAll("p, div");
for (let i = 0; i < targetAll.length; i++) {
  targetAll[i].style.backgroundColor = 'black';
  targetAll[i].style.color = 'white';
}

var targetAllTables = document.querySelectorAll("th, td");
for (let i = 0; i < targetAllTables.length; i++) {
  targetAllTables[i].style.backgroundColor = 'black';
  targetAllTables[i].style.color = 'indianRed';
}

var allPreEl = document.querySelectorAll('pre');
for (each of allPreEl) {
  each.style.backgroundColor = 'rgb(33,40,54)';
}

var allPreElChildren = document.querySelectorAll('pre > *');
for (each of allPreElChildren) {
  each.style.color = 'rgb(165,181,205)'
}

var allPreElChildren2 = document.querySelectorAll('pre > * > *');
for (each of allPreElChildren2) {
  each.style.color = 'orange'
}

var allPreElChildren3 = document.querySelectorAll('pre > * > * > *');
for (each of allPreElChildren3) {
  each.style.color = 'red'
}

var allPreElChildren4 = document.querySelectorAll('pre > * > * > * > *');
for (each of allPreElChildren4) {
  each.style.color = 'yellow'
}
})()
