# show styleized pwd & dir contents

clear;echo '\033[5;30;1m''>' '\033[0;22;32m'$(pwd) '\033[0;5;30;1m''<''\033[0m';echo '  ↳';CLICOLOR_FORCE=1 ls -1pG | sed 's/^/   ∙ /'; echo '';

# > /Users/cameron/Desktop/CODE/Repositories/Investment-Research-Platform <
#   ↳
#    ∙ Dockerfile
#    ∙ Icon
#    ∙ KEYS/
#    ∙ README.md
#    ∙ client/
#    ∙ db/
#    ∙ node_modules/
#    ∙ package-lock.json
#    ∙ package.json
#    ∙ server/
#    ∙ webpack.config.js
