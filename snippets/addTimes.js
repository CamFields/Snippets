const timeNodes = Array.from(document.querySelectorAll(/* elements containing times to accumulate */)).forEach((item, i, arr) => {arr[i] = item.innerHTML})

const seconds = timeNodes.reduce((totalSeconds, videoSeconds) => {
  let [mins, secs] = videoSeconds.split(':').map(parseFloat);
  videoSeconds = (mins * 60) + secs;
  return totalSeconds + videoSeconds;
},0);

function parseSeconds(seconds) {
  let secondsLeft = seconds;
  const hours = Math.floor(seconds / 3600);
  secondsLeft = seconds % 3600;

  const minutes = Math.floor(secondsLeft / 60);
  secondsLeft = secondsLeft % 60;

  return `${hours}:${minutes}:${secondsLeft}`
}

parseSeconds(seconds)
